import 'package:ewally_coin/api/model/error_model.dart';
import 'package:ewally_coin/api/user/model/login_response_model.dart';
import 'package:ewally_coin/modules/login/login_controller.dart';
import 'package:ewally_coin/modules/login/login_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';

import '../../util/base_test.dart';

class LoginViewModelTest extends BaseTest {
  LoginViewModelTest({bool skip}) : super(typeTest: TestTypes.viewModel, skip: skip);

  LoginController controller;
  LoginViewModel viewModel;

  @override
  init() {
    controller = MockLoginController();
    Get.put<LoginController>(controller);
    viewModel = LoginViewModel();
    super.init();
  }

  @override
  void tests() {
    test('should change username', () {
      expect(viewModel.username, null);
      viewModel.username = '46404034883';
      expect(viewModel.username, '46404034883');
    });
    test('should change usernameError', () {
      expect(viewModel.usernameError, null);
      viewModel.usernameError = 'Erro';
      expect(viewModel.usernameError, 'Erro');
      expect(viewModel.hasError, true);
    });
    test('should change password', () {
      expect(viewModel.password, null);
      viewModel.password = '46404034883';
      expect(viewModel.password, '46404034883');
    });
    test('should change passwordError', () {
      expect(viewModel.passwordError, null);
      viewModel.passwordError = 'Erro';
      expect(viewModel.passwordError, 'Erro');
      expect(viewModel.hasError, true);
    });

    test('Should do logn', () async {
      registerMockCounter(
        () => controller.doLogin(any, any),
        Future.value(LoginResponseModel(token: 'hahahakhdsaikjshd')),
      );

      expect(getMockCounter('doLogin'), 0);
      await viewModel.doLogin(notify: () => print('test'));
      expect(getMockCounter('doLogin'), 1);
    });
    test('Should not do logn', () async {
      resetAllMockCounter();
      registerMockCounter(
        () => controller.doLogin(any, any),
        Future.value(ErrorModel(code: 10101, msg: 'aiushdajsbd')),
        answerType: MockCounterType.thrown,
      );

      expect(getMockCounter('doLogin'), 0);
      await viewModel.doLogin(notify: () => print('test'));
      expect(getMockCounter('doLogin'), 0);
    });
  }
}

class MockLoginController extends Mock implements LoginController {}
