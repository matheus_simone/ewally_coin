import '../../api/account/account_repository.dart';
import '../../api/account/account_service.dart';
import '../../api/coin/coin_repository.dart';
import '../../api/coin/coin_service.dart';
import '../../api/http_service/http_service.dart';
import '../../api/user/user_repository.dart';
import '../../api/user/user_service.dart';
import 'home_controller.dart';
import 'home_view_model.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    final HttpService httpService = HttpService();
    Get.put<HomeController>(
      HomeController(
        userRepository: UserRepository(
          userService: UserService(
            httpService: httpService,
          ),
        ),
        accountRepository: AccountRepository(
          accountService: AccountService(httpService: httpService),
        ),
        coinRepository: CoinRepository(
          service: CoinService(
            httpService: httpService,
          ),
        ),
      ),
    );

    Get.put<HomeViewModel>(HomeViewModel());
  }
}
