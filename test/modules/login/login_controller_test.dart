import 'package:ewally_coin/api/model/error_model.dart';
import 'package:ewally_coin/api/user/model/login_response_model.dart';
import 'package:ewally_coin/api/user/user_repository.dart';
import 'package:ewally_coin/modules/login/login_controller.dart';
import 'package:ewally_coin/util/bearer_token.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../util/base_test.dart';
import '../../util/mocks.dart';

class LoginControllerTest extends BaseTest {
  LoginControllerTest({bool skip}) : super(typeTest: TestTypes.controller, skip: skip);

  final UserRepository userRepository = MockUserRepository();
  final BearerToken bearerToken = MockBearerToken();
  LoginController controller;

  @override
  void init() {
    controller = LoginController(userRepository: userRepository);
  }

  @override
  void tests() {
    test('should do login', () async {
      final response = LoginResponseModel(token: 'asuhdakjsbdiasyudgajdh');
      registerMockCounter(() => userRepository.doLogin(any), Future.value(response));

      expect(getMockCounter('doLogin'), 0);
      await controller.doLogin('anyUsername', 'anyPassword');
      expect(getMockCounter('doLogin'), 1);
      expect(bearerToken.token, response.token);
      bearerToken.setToken(null);
    });
    test('should not do login', () async {
      resetAllMockCounter();
      final response = ErrorModel(code: 190, msg: 'puliça, ajuda');
      registerMockCounter(
        () => userRepository.doLogin(any),
        response,
        answerType: MockCounterType.thrown,
      );
      try {
        await controller.doLogin('anyUsername', 'anyPassword');
      } catch (e) {
        expect(e.code, response.code);
        expect(e.msg, response.msg);
      }
      expect(bearerToken.token, null);
    });
  }
}
