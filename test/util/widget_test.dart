import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WidgetTest extends StatelessWidget {
  final dynamic pageInstance;
  WidgetTest({this.pageInstance});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: pageInstance,
      locale: Locale('pt', 'BR'),
    );
  }
}
