import 'package:flutter/material.dart';

final ColorScheme theme = ColorScheme(
  background: Color(0xff616161),
  onBackground: Colors.white,
  brightness: Brightness.dark,
  error: Colors.red,
  onError: Colors.white,
  primary: Colors.cyan[700],
  secondary: Colors.cyan[800],
  onPrimary: Colors.white,
  onSecondary: Colors.white,
  onSurface: Colors.black,
  primaryVariant: Colors.cyan[800],
  secondaryVariant: Colors.cyan[900],
  surface: Colors.white,
);

final TextTheme textTheme = ThemeData.dark().textTheme;

final TextStyle kTitle = TextStyle(fontSize: 30);

final TextStyle kLabel = TextStyle(fontSize: 20, fontWeight: FontWeight.w600);
final TextStyle kValue = TextStyle(fontSize: 22);
