import 'package:intl/intl.dart';

class Utils {
  static String maskBalance(int balance) {
    bool isNegative = balance < 0;
    if (isNegative) balance = balance * -1;
    String maskedBalance = (balance / 100).toStringAsFixed(2).split('.').map((e) => e.split(RegExp(r'(?=(?:...)*$)')).join('.')).join(',');
    return '${isNegative ? '-' : ''}$maskedBalance';
  }

  static String formatDate(DateTime date, [String pattern = 'dd/MM/yyyy HH:mm']) {
    return DateFormat(pattern).format(date.toLocal());
  }
}
