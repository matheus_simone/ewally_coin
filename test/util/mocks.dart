import 'package:ewally_coin/api/account/account_repository.dart';
import 'package:ewally_coin/api/coin/coin_repository.dart';
import 'package:ewally_coin/util/bearer_token.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';
import 'package:ewally_coin/api/user/user_repository.dart';

class MockBearerToken extends Mock implements BearerToken {
  MockBearerToken._();

  static final MockBearerToken instance = MockBearerToken._();

  factory MockBearerToken() => Get.find<BearerToken>();

  String _token;

  void setToken(String bearer) => _token = bearer;

  String get token => _token;
}

class MockUserRepository extends Mock implements UserRepository {}

class MockAccountRepository extends Mock implements AccountRepository {}

class MockCoinRepository extends Mock implements CoinRepository {}
