import '../../model/base_model_interface.dart';

class BalanceResponseModel implements BaseModelInterface {
  int balance;
  int blockedBalance;

  BalanceResponseModel({this.balance, this.blockedBalance});

  BalanceResponseModel fromJson(Map<String, dynamic> json) {
    balance = json['balance'];
    blockedBalance = json['blockedBalance'];
    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['balance'] = this.balance;
    data['blockedBalance'] = this.blockedBalance;
    return data;
  }
}
