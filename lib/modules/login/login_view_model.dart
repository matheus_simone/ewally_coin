import '../../api/model/error_model.dart';
import 'login_controller.dart';
import 'package:get/get.dart';
import '../../util/extensions.dart';

class LoginViewModel extends GetxController {
  final LoginController controller = Get.find<LoginController>();

  String username;
  String usernameError;
  String password;
  String passwordError;
  bool loading = false;

  bool get hasError =>
      (this.username.isNullOrEmpty || !this.usernameError.isNullOrEmpty) ||
      (this.password.isNullOrEmpty || !this.passwordError.isNullOrEmpty);

  Future doLogin({Function notify}) async {
    try {
      loading = true;
      notify();
      await controller.doLogin(username, password);
    } on ErrorModel catch (errorModel) {
      print(errorModel.msg);
      Get.snackbar('Erro', errorModel.msg);
    } catch (error) {
      print(error);
      Get.snackbar('Erro', 'Erro genérico');
    } finally {
      loading = false;
      notify();
    }
  }
}
