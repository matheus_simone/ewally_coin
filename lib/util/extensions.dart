extension NullOrEmpty on String {
  bool get isNullOrEmpty => this == null || this.isEmpty;
}

extension LocalDate on int {
  DateTime get localDateTime => DateTime.fromMillisecondsSinceEpoch(this).toLocal();
}
