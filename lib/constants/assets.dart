class Assets {
  static final String _assetsFolder = 'assets';
  static final String _imagesFolder = '$_assetsFolder/images';

  final String logo = '$_imagesFolder/logo.svg';
}
