import '../../api/user/model/login_request_model.dart';
import '../../api/user/model/login_response_model.dart';
import '../../api/user/user_repository.dart';
import '../../util/bearer_token.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../util/extensions.dart';

class LoginController extends GetxController {
  final UserRepository userRepository;
  final BearerToken bearerToken = BearerToken();

  LoginController({@required this.userRepository});

  Future doLogin(String username, String password) async {
    LoginResponseModel response = await userRepository.doLogin(LoginRequestModel(username: username, password: password));
    if (!response.token.isNullOrEmpty) {
      bearerToken.setToken(response.token);
      Get.offAllNamed('/home');
    }
  }
}
