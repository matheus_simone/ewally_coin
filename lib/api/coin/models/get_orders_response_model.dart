import 'coin_order_response_model.dart';
import '../../model/base_model_interface.dart';

class GetOrdersResponseModel extends BaseModelInterface {
  List<CoinOrder> orders;

  GetOrdersResponseModel({this.orders});

  GetOrdersResponseModel fromJson(Map<String, dynamic> json) {
    if (json['orders'] != null) {
      orders = <CoinOrder>[];
      json['orders'].forEach((v) {
        orders.add(new CoinOrder().fromJson(v));
      });
    }

    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orders != null) {
      data['orders'] = this.orders.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
