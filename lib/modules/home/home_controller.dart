import '../../api/account/account_repository.dart';
import '../../api/account/models/balance_response_model.dart';
import '../../api/coin/coin_repository.dart';
import '../../api/coin/models/get_orders_response_model.dart';
import '../../api/user/user_repository.dart';
import 'package:get/get.dart';

class HomeController {
  final AccountRepository accountRepository;
  final UserRepository userRepository;
  final CoinRepository coinRepository;

  HomeController({this.accountRepository, this.coinRepository, this.userRepository});

  Future<BalanceResponseModel> getBalance() => accountRepository.balance();

  Future<GetOrdersResponseModel> getCoinOrders() => coinRepository.getOrders();

  Future logout() async {
    await userRepository.logout();
    Get.offNamedUntil('/login', (route) => false);
  }
}
