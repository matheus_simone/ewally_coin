import 'package:ewally_coin/util/bearer_token.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

import 'modules/modules_test.dart';
import 'util/mocks.dart';

void main() {
  globalInit();
  ModulesTest();
}

void globalInit() {
  TestWidgetsFlutterBinding.ensureInitialized();
  Get.testMode = true;
  Get.put<BearerToken>(MockBearerToken.instance);
}
