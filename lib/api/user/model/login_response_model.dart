import '../../model/base_model_interface.dart';

class LoginResponseModel extends BaseModelInterface {
  String token;

  LoginResponseModel({this.token});

  @override
  BaseModelInterface fromJson(Map<String, dynamic> json) {
    this.token = json['token'];

    return this;
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    json['token'] = this.token;

    return json;
  }
}
