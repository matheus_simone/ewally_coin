import '../../model/base_model_interface.dart';

class CoinOrder extends BaseModelInterface {
  String id;
  DateTime createAt;
  int price;
  int currencyAmount;
  int totalAmount;
  String currencyCode;
  String currencyName;
  String status;
  String nsu;
  String cupom;

  CoinOrder(
      {this.id,
      this.createAt,
      this.price,
      this.currencyAmount,
      this.totalAmount,
      this.currencyCode,
      this.currencyName,
      this.status,
      this.nsu,
      this.cupom});

  CoinOrder fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createAt = DateTime.fromMillisecondsSinceEpoch(json['createAt']);
    price = json['price'];
    currencyAmount = json['currencyAmount'];
    totalAmount = json['totalAmount'];
    currencyCode = json['currencyCode'];
    currencyName = json['currencyName'];
    status = json['status'];
    nsu = json['nsu'];
    cupom = json['cupom'];

    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['createAt'] = this.createAt;
    data['price'] = this.price;
    data['currencyAmount'] = this.currencyAmount;
    data['totalAmount'] = this.totalAmount;
    data['currencyCode'] = this.currencyCode;
    data['currencyName'] = this.currencyName;
    data['status'] = this.status;
    data['nsu'] = this.nsu;
    data['cupom'] = this.cupom;
    return data;
  }
}
