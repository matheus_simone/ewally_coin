import '../../api/account/models/balance_response_model.dart';
import '../../api/coin/models/coin_order_response_model.dart';
import '../../api/coin/models/get_orders_response_model.dart';
import '../../api/model/error_model.dart';
import '../../interface/coin_interface.dart';
import 'home_controller.dart';
import 'package:get/get.dart';

class HomeViewModel extends GetxController {
  final HomeController controller = Get.find<HomeController>();

  List<Coin> get coins => [
        Coin(code: '840', name: 'USD'),
        Coin(code: '978', name: 'EUR'),
      ];

  List<CoinOrder> coinOrders;
  bool coinOrdersLoading = false;

  int balance;
  bool balanceLoading = false;

  Future getBalance({Function notify}) async {
    try {
      balanceLoading = true;
      notify();

      BalanceResponseModel response = await controller.getBalance();
      balance = response.balance;
    } on ErrorModel catch (errorModel) {
      print(errorModel.msg);
      Get.snackbar('Erro', errorModel.msg);
    } catch (error) {
      print(error);
      Get.snackbar('Erro', 'Erro genérico');
    } finally {
      balanceLoading = false;
      notify();
    }
  }

  Future getOrders({Function notify}) async {
    try {
      coinOrdersLoading = true;
      notify();
      GetOrdersResponseModel response = await controller.getCoinOrders();
      coinOrders = response.orders;
    } on ErrorModel catch (errorModel) {
      print(errorModel.msg);
      Get.snackbar('Erro', errorModel.msg);
    } catch (error) {
      print(error);
      Get.snackbar('Erro', 'Erro genérico');
    } finally {
      coinOrdersLoading = false;
      notify();
    }
  }

  Future logout() async {
    try {
      await controller.logout();
    } on ErrorModel catch (errorModel) {
      print(errorModel.msg);
      Get.snackbar('Erro', errorModel.msg);
    } catch (error) {
      print(error);
      Get.snackbar('Erro', 'Erro genérico');
    }
  }
}
