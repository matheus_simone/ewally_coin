import 'package:get/get.dart';
import 'errors_map.dart';

class ErrorModel {
  int code;
  int http;
  String _msg;
  ErrorsType type;
  List<String> args;

  String get msg => _msg.trArgs(args);

  set msg(String msg) {
    _msg = msg;
  }

  ErrorModel({this.code, msg = '', this.type, this.args = const [], this.http}) : assert(code != null) {
    type ??= ErrorsType.baseUnknown;
    _msg = msg;
  }

  Map<String, dynamic> toJson() {
    return {'code': code, 'msg': msg, 'http': http};
  }

  ErrorModel fromJson({Map<String, dynamic> json, int http}) {
    code = json['code'];
    _msg = json['msg'];
    this.http = http ?? 0;
    return this;
  }
}
