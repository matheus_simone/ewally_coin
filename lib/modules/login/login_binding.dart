import '../../api/http_service/http_service.dart';
import '../../api/user/user_repository.dart';
import '../../api/user/user_service.dart';
import 'login_controller.dart';
import 'login_view_model.dart';
import 'package:get/get.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<LoginController>(
      LoginController(
        userRepository: UserRepository(
          userService: UserService(
            httpService: HttpService(),
          ),
        ),
      ),
    );

    Get.put<LoginViewModel>(LoginViewModel());
  }
}
