import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../api/coin/models/coin_order_response_model.dart';
import '../../constants/theme.dart';
import '../../interface/coin_interface.dart';
import '../../util/utils.dart';
import 'home_view_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeViewModel viewModel = Get.find<HomeViewModel>();

  @override
  void initState() {
    super.initState();
    callInit();
  }

  void callInit() async {
    final notify = () => setState(() {});
    await Future.wait([
      viewModel.getBalance(notify: notify),
      viewModel.getOrders(notify: notify),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.primary,
        elevation: 0,
        centerTitle: false,
        title: Text('Ewally Coin'),
        actions: [
          IconButton(
            icon: Icon(Icons.login_sharp),
            onPressed: viewModel.logout,
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size(Get.width, 70),
          child: Container(
            height: 70,
            width: Get.width,
            child: Center(
              child: viewModel.balanceLoading
                  ? CircularProgressIndicator.adaptive()
                  : Text(
                      'R\$ ${Utils.maskBalance(viewModel.balance ?? 0)}',
                      style: kTitle,
                    ),
            ),
          ),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        'Moedas',
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: viewModel.coins.map((coin) => getCard(coin)).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: viewModel.coinOrdersLoading
                    ? CircularProgressIndicator.adaptive()
                    : viewModel.coinOrders == null || viewModel.coinOrders.isEmpty
                        ? Center(
                            child: Text('Não há pedidos'),
                          )
                        : ListView.builder(
                            itemCount: 5,
                            itemBuilder: (context, index) {
                              if (viewModel.coinOrders.length < index + 1) return SizedBox();
                              final CoinOrder order = viewModel.coinOrders[index];
                              return ListTile(
                                title: Text(
                                  order.currencyName,
                                  style: kValue,
                                ),
                                subtitle: Text(Utils.formatDate(order.createAt)),
                                trailing: Text(
                                  'R\$ ${Utils.maskBalance(order.totalAmount)}',
                                  style: kLabel,
                                ),
                              );
                            }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getCard(Coin coin) {
    return GestureDetector(
      onTap: () {
        print(coin.code);
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            colors: [theme.primary, theme.secondaryVariant],
          ),
        ),
        height: 200,
        width: Get.width * 0.8,
        child: Center(
          child: Text(
            coin.name,
            style: kTitle,
          ),
        ),
      ),
    );
  }
}
