import 'error_model.dart';

enum ErrorsType {
  baseUnknown,
  baseServerResponse,
  baseInvalidResponse,
  baseInvalidCertificate,
  baseConnectionFailure,
  baseConnectionMaxRetriesFailure,
  baseInvalidMinimumVersion,
}

class ErrorsMap {
  static Map<ErrorsType, ErrorModel> errors = {
    ErrorsType.baseUnknown: ErrorModel(code: 0, msg: 'error.base.unknown', http: 0),
    ErrorsType.baseServerResponse: ErrorModel(code: 0, msg: '', http: 1),
    ErrorsType.baseInvalidResponse: ErrorModel(code: 0, msg: 'error.base.invalid.response', http: 2),
    ErrorsType.baseConnectionFailure: ErrorModel(code: 0, msg: 'error.base.connection.failure', http: 7),
    ErrorsType.baseConnectionMaxRetriesFailure: ErrorModel(code: 0, msg: 'error.base.connection.retries', http: 8),
    ErrorsType.baseInvalidMinimumVersion: ErrorModel(code: 0, msg: 'error.base.invalid.version', http: 170),
    ErrorsType.baseInvalidCertificate: ErrorModel(code: 0, msg: 'error.base.invalid.certificate', http: 171),
  };

  static ErrorModel getError({ErrorsType errorsType, List<String> args = const [], int http, int code = 0}) {
    ErrorModel error = ErrorsMap.errors[errorsType];
    if (error == null)
      return getError(
        errorsType: ErrorsType.baseUnknown,
        args: [ErrorsType.baseUnknown.toString()],
        code: code ?? 0,
      );
    error.args = args;
    error.http = http ?? error.http;
    error.type = errorsType;
    error.code = code;
    return error;
  }
}
