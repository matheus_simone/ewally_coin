import '../../model/base_model_interface.dart';

class LoginRequestModel extends BaseModelInterface {
  String username;
  String password;

  LoginRequestModel({this.username, this.password});

  LoginRequestModel fromJson(Map<String, dynamic> json) {
    username = json['username'];
    password = json['password'];
    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
