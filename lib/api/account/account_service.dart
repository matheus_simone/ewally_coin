import 'models/balance_response_model.dart';
import '../http_service/http_service.dart';

class AccountService {
  final HttpService httpService;

  AccountService({this.httpService});

  Future<BalanceResponseModel> balance() {
    return httpService.request(
      type: RequestType.GET,
      path: '/account/balance',
      dataResponse: BalanceResponseModel(),
    );
  }
}
