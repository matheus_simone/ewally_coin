import '../http_service/http_service.dart';
import 'model/login_request_model.dart';
import 'model/login_response_model.dart';
import 'package:flutter/material.dart';

class UserService {
  final HttpService httpService;
  UserService({@required this.httpService});

  Future<LoginResponseModel> doLogin(LoginRequestModel loginData) => httpService.request(
        path: '/user/login',
        dataRequest: loginData,
        dataResponse: LoginResponseModel(),
        type: RequestType.POST,
      );

  Future logout() => httpService.request(
        path: '/user/logout/session',
        type: RequestType.POST,
      );
}
