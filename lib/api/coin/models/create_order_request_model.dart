import '../../model/base_model_interface.dart';

class CreateOrderRequestModel extends BaseModelInterface {
  String priceId;
  int currencyAmount;

  CreateOrderRequestModel({this.priceId, this.currencyAmount});

  CreateOrderRequestModel fromJson(Map<String, dynamic> json) {
    priceId = json['priceId'];
    currencyAmount = json['currencyAmount'];

    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['priceId'] = this.priceId;
    data['currencyAmount'] = this.currencyAmount;
    return data;
  }
}
