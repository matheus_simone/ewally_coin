import '../../constants/assets.dart';
import '../../constants/theme.dart';
import 'login_view_model.dart';
import '../../util/extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'package:easy_mask/easy_mask.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final Assets assets = Assets();
  final LoginViewModel viewModel = Get.find<LoginViewModel>();

  void _onUsernameChanged(String _username) {
    String replaced = _username.replaceAll(new RegExp(r'\D'), '');
    viewModel.username = replaced;
    viewModel.usernameError = !replaced.isNullOrEmpty && replaced.length < 11 ? 'Nome de usuário inválido' : null;
    setState(() {});
  }

  void _onPasswordChanged(String _password) {
    viewModel.password = _password;
    viewModel.passwordError = !_password.isNullOrEmpty && _password.length < 6 ? 'Senha invalida' : null;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.focusScope.unfocus();
      },
      child: Scaffold(
        body: Container(
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: Get.height * 0.08),
                child: Center(
                    child: SvgPicture.asset(
                  assets.logo,
                  width: 50,
                )),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: Get.height * 0.05),
                padding: const EdgeInsets.all(16),
                width: Get.width,
                height: Get.height * 0.45,
                decoration: BoxDecoration(
                  color: theme.background,
                  borderRadius: BorderRadius.all(
                    Radius.circular(4),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InputField(
                      onChanged: _onUsernameChanged,
                      label: 'Nome de usuário',
                      inputType: TextInputType.number,
                      error: viewModel.usernameError,
                      formatters: [
                        TextInputMask(
                          mask: ['999.999.999-99', '99.999.999/9999-99'],
                        )
                      ],
                    ),
                    InputField(
                      onChanged: _onPasswordChanged,
                      label: 'Senha',
                      obscureText: true,
                      error: viewModel.passwordError,
                    )
                  ],
                ),
              ),
              Container(
                child: CupertinoButton.filled(
                  child: viewModel.loading ? CircularProgressIndicator.adaptive() : Text('Entrar'),
                  onPressed: viewModel.hasError || viewModel.loading ? null : () => viewModel.doLogin(notify: () => setState(() {})),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class InputField extends StatelessWidget {
  InputField({
    this.controller,
    this.height = 48,
    this.inputType,
    this.label,
    this.obscureText = false,
    this.formatters,
    this.error,
    this.onChanged,
  });

  final TextEditingController controller;
  final double height;
  final TextInputType inputType;
  final String label;
  final bool obscureText;
  final List<TextInputFormatter> formatters;
  final String error;
  final Function(String) onChanged;

  bool get hasError => !this.error.isNullOrEmpty;

  double get finalHeight => this.height + (hasError ? 20 : 0);

  InputBorder get border => UnderlineInputBorder(
        borderSide: BorderSide(color: hasError ? theme.error : theme.secondary),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: finalHeight,
      child: TextField(
        onChanged: this.onChanged,
        obscureText: this.obscureText,
        style: TextStyle(color: hasError ? theme.error : Colors.black),
        controller: this.controller,
        keyboardType: this.inputType,
        inputFormatters: this.formatters,
        decoration: InputDecoration(
          errorText: this.error,
          labelText: this.label,
          labelStyle: TextStyle(color: hasError ? theme.error : Colors.black),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: const EdgeInsets.all(8),
          filled: true,
          fillColor: Colors.white,
          border: border,
        ),
      ),
    );
  }
}
