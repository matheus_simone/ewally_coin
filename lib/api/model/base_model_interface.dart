abstract class BaseModelInterface {
  BaseModelInterface fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson();
}
