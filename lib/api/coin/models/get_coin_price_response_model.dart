import '../../model/base_model_interface.dart';
import '../../../util/extensions.dart';

class GetCoinPriceResponseModel extends BaseModelInterface {
  String currencyCode;
  String currencyName;
  int price;
  String id;
  DateTime expireAt;

  GetCoinPriceResponseModel({this.currencyCode, this.currencyName, this.price, this.id, this.expireAt});

  GetCoinPriceResponseModel fromJson(Map<String, dynamic> json) {
    currencyCode = json['currencyCode'];
    currencyName = json['currencyName'];
    price = json['price'];
    id = json['id'];
    expireAt = (json['expireAt'] as int).localDateTime;

    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currencyCode'] = this.currencyCode;
    data['currencyName'] = this.currencyName;
    data['price'] = this.price;
    data['id'] = this.id;
    data['expireAt'] = this.expireAt.millisecondsSinceEpoch;
    return data;
  }
}
