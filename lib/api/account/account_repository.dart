import 'account_service.dart';
import 'models/balance_response_model.dart';

class AccountRepository {
  final AccountService accountService;

  AccountRepository({this.accountService});

  Future<BalanceResponseModel> balance() => accountService.balance();
}
