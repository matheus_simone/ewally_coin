import 'model/login_request_model.dart';
import 'model/login_response_model.dart';
import 'user_service.dart';

class UserRepository {
  final UserService userService;

  UserRepository({this.userService});

  Future<LoginResponseModel> doLogin(LoginRequestModel loginData) => userService.doLogin(loginData);

  Future logout() => userService.logout();
}
