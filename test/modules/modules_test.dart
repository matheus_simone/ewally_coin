import '../util/base_test.dart';
import 'login/login_test.dart';

class ModulesTest extends BaseTest {
  ModulesTest({bool skip}) : super(typeTest: 'MODULES', skip: skip);

  @override
  void tests() {
    LoginTest();
  }
}
