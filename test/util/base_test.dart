import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

enum MockCounterType { normal, thrown }

abstract class BaseTest {
  String typeTest = 'BASE';
  Map<String, int> _mockCallCount = {};

  BaseTest({this.typeTest, bool skip = false}) {
    if (skip == null || !skip) this.execute();
  }

  String get goldenPath => './golden';

  void registerMockCounter(Function function, dynamic expectedResponse,
      {MockCounterType answerType = MockCounterType.normal, String functionKeyName}) {
    if (answerType == MockCounterType.normal)
      when(Function.apply(function, []))
          .thenAnswer((realInvocation) => addMockCounter(functionKeyName ?? realInvocation, expectedResponse));

    if (answerType == MockCounterType.thrown) when(Function.apply(function, [])).thenThrow(expectedResponse);
  }

  dynamic addMockCounter(dynamic invocation, dynamic returnValue) {
    String typeName = '';
    if (invocation is String) {
      typeName = invocation;
    } else {
      typeName = invocation.memberName.toString().split('"')[1];
    }

    _mockCallCount[typeName] ??= 0;
    _mockCallCount[typeName] += 1;
    return returnValue;
  }

  int getMockCounter(String type) => _mockCallCount[type] ?? 0;
  void clearMockCounter(String type) => _mockCallCount[type] = 0;
  void resetAllMockCounter() => _mockCallCount.clear();
  Map<String, int> getAllCounter() => _mockCallCount;

  void execute() {
    group('[$typeTest] -', () {
      test('INIT', init);

      tests();
      test('FINALIZE', finalize);
      resetMockitoState();
    });
  }

  init() {}

  finalize() {
    resetAllMockCounter();
  }

  tests();
}

class TestTypes {
  static final String page = 'PAGE';
  static final String viewModel = 'VIEW_MODEL';
  static final String controller = 'CONTROLLER';
  static final String widget = 'WIDGET';
}
