import 'dart:async';

import 'package:ewally_coin/util/bearer_token.dart';

import 'modules/home/home_binding.dart';
import 'modules/home/home_page.dart';
import 'modules/login/login_binding.dart';
import 'modules/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  Get.put<BearerToken>(BearerToken.instance);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ewally Coin',
      darkTheme: ThemeData.dark().copyWith(
        colorScheme: ColorScheme(
          background: Color(0xff616161),
          onBackground: Colors.white,
          brightness: Brightness.dark,
          error: Colors.red,
          onError: Colors.white,
          primary: Colors.cyan[700],
          secondary: Colors.cyan[800],
          onPrimary: Colors.white,
          onSecondary: Colors.white,
          onSurface: Colors.black,
          primaryVariant: Colors.cyan[800],
          secondaryVariant: Colors.cyan[900],
          surface: Colors.white,
        ),
      ),
      themeMode: ThemeMode.dark,
      getPages: [
        GetPage(name: '/', page: () => SplashPage()),
        GetPage(
          name: '/login',
          page: () => LoginPage(),
          binding: LoginBinding(),
        ),
        GetPage(
          name: '/home',
          page: () => HomePage(),
          binding: HomeBinding(),
        ),
      ],
      initialRoute: '/',
    );
  }
}

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(Duration(milliseconds: 2000), () {
      Get.offAllNamed('/login');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Icon(
          Icons.access_alarm_sharp,
          size: Get.width / 2,
        ),
      ),
    );
  }
}
