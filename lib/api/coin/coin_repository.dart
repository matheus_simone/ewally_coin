import 'coin_service.dart';
import 'models/coin_order_response_model.dart';
import 'models/create_order_request_model.dart';
import 'models/get_coin_price_response_model.dart';
import 'models/get_orders_response_model.dart';

class CoinRepository {
  final CoinService service;

  CoinRepository({this.service});

  Future<GetCoinPriceResponseModel> getPrice(String code) => service.getPrice(code);

  Future<GetOrdersResponseModel> getOrders() => service.getOrders();

  Future<CoinOrder> createOrder(CreateOrderRequestModel request) => service.createOrder(request);
}
