import 'models/coin_order_response_model.dart';
import 'models/create_order_request_model.dart';
import 'models/get_coin_price_response_model.dart';
import 'models/get_orders_response_model.dart';
import '../http_service/http_service.dart';

class CoinService {
  final HttpService httpService;

  CoinService({this.httpService});

  Future<GetCoinPriceResponseModel> getPrice(String code) => httpService.request(
        path: '/ewallycoin/price/$code',
        type: RequestType.GET,
        dataResponse: GetCoinPriceResponseModel(),
      );

  Future<GetOrdersResponseModel> getOrders() => httpService.request(
        path: '/ewallycoin/order',
        type: RequestType.GET,
        dataResponse: GetOrdersResponseModel(),
      );

  Future<CoinOrder> createOrder(CreateOrderRequestModel request) => httpService.request(
        path: '/ewallycoin/order',
        type: RequestType.POST,
        dataRequest: request,
        dataResponse: CoinOrder(),
      );
}
