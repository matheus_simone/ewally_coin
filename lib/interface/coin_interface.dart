import '../api/coin/models/get_coin_price_response_model.dart';

class Coin {
  String name;
  String code;
  int price;
  String id;
  DateTime expireAt;

  Coin({this.name, this.code, this.price, this.id, this.expireAt});

  Coin.fromResponseModel(GetCoinPriceResponseModel responseModel) {
    this.name = responseModel.currencyName;
    this.code = responseModel.currencyCode;
    this.price = responseModel.price;
    this.id = responseModel.id;
    this.expireAt = responseModel.expireAt;
  }
}
