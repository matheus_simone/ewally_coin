import 'package:get/get.dart';

class BearerToken {
  BearerToken._();

  factory BearerToken() => Get.find<BearerToken>();

  static final BearerToken instance = BearerToken._();

  String _token;

  void setToken(String bearer) => _token = bearer;

  String get token => _token;
}
