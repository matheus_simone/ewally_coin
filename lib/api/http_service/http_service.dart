import 'dart:io';

import 'package:dio/dio.dart';
import '../model/base_model_interface.dart';
import '../model/error_model.dart';
import '../model/errors_map.dart';
import '../../util/bearer_token.dart';
import 'package:get/get_utils/get_utils.dart';

enum RequestType { GET, POST, PUT, PATCH, DELETE }

class HttpService {
  Dio _dio;

  HttpService() {
    init();
  }

  final BearerToken bearerToken = BearerToken();

  void init() {
    if (_dio == null) {
      _dio = Dio(
        BaseOptions(
          baseUrl: 'https://ms.apidev.ewally.com.br',
          validateStatus: (status) => status < 400,
          receiveTimeout: 20000,
        ),
      );
      _dio.interceptors.add(InterceptorsWrapper(
        onRequest: (options) {
          if (bearerToken.token != null) {
            options.headers.addAll(
              {'Authorization': 'Bearer ${bearerToken.token}'},
            );
          }
        },
      ));
    }
  }

  Future<T> request<T>({
    RequestType type,
    String path,
    BaseModelInterface dataResponse,
    BaseModelInterface dataRequest,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      Response res = await _dio.request(
        path,
        data: dataRequest?.toJson(),
        queryParameters: queryParameters,
        options: _checkOptions(type.toString().split('.')[1], options),
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      print(res.data);

      return dataResponse?.fromJson(res?.data) as T ?? res?.data as T;
    } catch (error) {
      print(error);
      if ((error?.type == DioErrorType.RECEIVE_TIMEOUT ||
              error?.type == DioErrorType.CONNECT_TIMEOUT ||
              error?.response?.statusMessage == 'Gateway Timeout') ??
          false) {
        ErrorModel errorModel = ErrorsMap.getError(errorsType: ErrorsType.baseServerResponse);
        errorModel = errorModel.fromJson(
          json: {
            'code': 408,
            'msg': 'error.http.408'.tr,
          },
          http: 408,
        );
        throw errorModel;
      }
      if (error?.response?.data != null) {
        ErrorModel errorModel = ErrorsMap.getError(errorsType: ErrorsType.baseServerResponse);
        errorModel = errorModel.fromJson(
          json: error?.response?.data ?? {},
          http: error?.response?.statusCode,
        );
        throw errorModel;
      } else {
        if (error?.usernameError?.runtimeType == HandshakeException) {
          throw ErrorsMap.getError(errorsType: ErrorsType.baseInvalidCertificate);
        }
        if (error?.usernameError?.runtimeType == SocketException) {
          throw ErrorsMap.getError(errorsType: ErrorsType.baseConnectionFailure);
        }
        if (error?.response?.statusCode != null)
          throw ErrorsMap.getError(errorsType: ErrorsType.baseInvalidResponse, http: error.response.statusCode);
        throw ErrorsMap.getError(errorsType: ErrorsType.baseUnknown);
      }
    }
  }

  Options _checkOptions(String method, options) {
    options ??= Options();
    options.method = method;
    return options;
  }
}
