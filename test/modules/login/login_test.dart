import '../../util/base_test.dart';
import 'login_controller_test.dart';
import 'login_page_test.dart';
import 'login_view_model_test.dart';

class LoginTest extends BaseTest {
  LoginTest({bool skip}) : super(typeTest: 'LOGIN', skip: skip);

  @override
  void tests() {
    LoginPageTest();
    LoginViewModelTest();
    LoginControllerTest();
  }
}
