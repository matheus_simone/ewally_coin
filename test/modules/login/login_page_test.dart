import 'package:ewally_coin/modules/login/login_page.dart';
import 'package:ewally_coin/modules/login/login_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';

import '../../util/base_test.dart';
import '../../util/widget_test.dart';

class LoginPageTest extends BaseTest {
  LoginPageTest({bool skip}) : super(typeTest: TestTypes.page, skip: skip);

  MockLoginViewModel viewModel;

  @override
  init() {
    viewModel = MockLoginViewModel();
    Get.put<LoginViewModel>(viewModel);
    super.init();
  }

  @override
  finalize() {
    Get.delete<LoginViewModel>();
    return super.finalize();
  }

  @override
  void tests() {
    testWidgets('GoldenTest - Login', (tester) async {
      await tester.pumpWidget(
        WidgetTest(
          pageInstance: LoginPage(),
        ),
      );
      await tester.pumpAndSettle();
      await expectLater(find.byType(LoginPage), matchesGoldenFile('$goldenPath/loginPage.png'));
    });
    testWidgets('GoldenTest - Login with error', (tester) async {
      viewModel.usernameError = 'ERRRRRRRRROOOOOOOR';
      viewModel.passwordError = 'ERRRRRRRRROOOOOOOR';
      viewModel.hasError = true;
      await tester.pumpWidget(
        WidgetTest(
          pageInstance: LoginPage(),
        ),
      );
      await expectLater(find.byType(LoginPage), matchesGoldenFile('$goldenPath/loginPageWithError.png'));
    });
  }
}

class MockLoginViewModel extends Mock implements LoginViewModel {
  bool loading = false;
  bool hasError = false;

  String usernameError;
  String passwordError;
}
